var request = require('request');           //require js file to write file from URL
var $ = require("./jquery.min.js");

var webview = document.getElementById("webview");
webview.addEventListener("dom-ready", function () {
    webview.openDevTools();
});

function checkOff(selector) {
    $(selector).addClass("done");
    $(selector).find("span").removeClass("hidden");
}

webview.addEventListener('ipc-message', function (event) {
    console.log(event);
    if (event.channel == "QR") {
        checkOff("#step1");
    }
    else if (event.channel == "login") {
        checkOff("#step1");
        checkOff("#step2");
    } else if (event.channel == "chat") {
        checkOff("#step1");
        checkOff("#step2");
        checkOff("#step3");
        $("#task").removeClass("hidden");
    }
})